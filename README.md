![CareSend](https://gitlab.com/pumposh/caresend-assessment-upcoming-events/-/raw/master/src/assets/logos/CareSend%20Horizontal.png)
# Upcoming Events

#### Introduction
Your assignment is to build a prototype of a Vue.js web app called “Upcoming Events”. The app must display a list of events, sectioned by start date, and listed in chronological order. The app must also correctly identify conflicts between events.

#### Getting Started

Fork or clone this repository and run the following commands to initialize your development environment: 

```bash
$ yarn
$ yarn serve
```

Within this repo in the directory `/src/assets/data/` is a JSON file titled `mock.json`. This file contains the input for this assignment: an array of `Event` objects that match the following schema:

```typescript
interface Event {
  title: string;
  start: string; // i.e. “November 6, 2018 1:26 PM”
  end: string;
}
```

Note that the events in the input file are not presorted chronologically. The application must interpret this data and meet the following requirements:

#### UI Requirements

In this project, SASS and Buefy are configured for your convenience, but you may install any style preprocessor or UI bootstrap framework you prefer as long as you provide an explanation for your preference.

- All event information must be displayed (title, start, end)
- Events are listed in chronological order
- Events are grouped by date
- There is some indication that an event conflicts (overlaps) with another event.

#### Functional Requirements

To implement the event conflict feature, you will need to design an algorithm to find events that overlap with each other. Note that an event ending at the same time another event starts should not be considered a conflict.

- Sort all events chronologically
- Design and implement an algorithm to determine event conflicts 
- Determining event conflicts in the entire data set must perform better than *O(n<sup>2</sup>)*
- Clear separation of concerns, good software architecture
- Comments with a brief description of the algorithm


#### Analytical Requirements

Edit this `README.md` in your forked repository and at the bottom of the document, provide an analysis of your implementation process with consideration of the following:

- Thought process while designing your application
- Explanation of additionally installed dependencies
- Trade-offs or assumptions you made in your design
- Run time complexity analysis


#### Submission
To submit your work, deploy your project to GitHub/GitLab and make sure to have your analysis within the `README.md` file. Provide a link to the working application (Heroku, Google Cloud, AWS or any other web hosting should work)

#### Tips & FAQs
- Don’t worry too much about UI design. You can use any bootstrap framework of your choice and keep it simple
- You can use any external libraries and dependencies as long as you can clearly explain how they work
- You can assume that events occur in your local timezone (i.e., you may ignore time zones for this assignment)
- An event ending at the same time another event starts should not be considered a conflict

### Project Analysis

Full Name: Elisson Espinal

#### Algorithm

Initially, I implemented a somewhat "naive" algorithm that performed right at *O(n<sup>2</sup>)*. The way it worked
is simple: sort the mock data (*O(n log(n))*), iterate through each event, keep track of each event from the past
that has not finished-up by the start time of the current event, and perform a **linear** lookup of any events
whose end-time overlaps (non-inclusive) with the current event in order to label them as a conflict. This last point
is where the implementation is bound to O(n<sup>2</sup>)*, since in the worst case where we aren't deleting any of
the past events, one must perform ~n(n-1)/2 operations. The optimization came once I realized one could use an
interval tree data structure - a more-or-less variation on the concept of a binary search tree that uses overlapping
comparisons as its main way to build itself - to reduce the look-up of overlapping events to *O(n log(n))*, leading
to a final average time complexity of *O(n log n )*.

#### UI + Architecture

Architecture wise, I kept things fairly simple and decided to sort the data and find conflicts right at the component level
since the data itself wasn't very large, and wanted to make sure I focused on learning Vue for the first time + implementing
a meaningful visualization of the data with the given tools. In a production settings, all the data processing would
preferably be hidden behind an API call to some backend code, or perhaps done by a server-side component (not sure
yet if this is possible in Vue, but I know Next.Js can do this by default.) With that out of the way, I wanted to
take as much of a "don't re-invent the wheel" approach as possible, using Buefy components and Bulma classes as
much as possible. I decided that I would got for two views: one being a traditional table-view, and the other being
a "time-line" style view in order to help _**feel**_ the overlaps between conflicting activities. Both of these would
be sorted chronologically and separated by day, with the user being able to switch between seeing a single day at a
time and a specific range. In addition, the user should be able to seamlessly transition between the two views. The first
approach was simple in concept, and the only difficult bits was learning to program things "vue-tifully" given
it is my first experience with the framework. The second approach was a bit tricky, since I realized the best way to
go was to use the closest available component for a 'timeline', a stacked progress bar, which lead to a series of
restrictions and consequent work-arounds that took some time to figure out. for example, the labels for each of the
events would not easily fit (and layer on top-of) the Buefy progress bars, and I had to make the little time ticks
out of divs since there weren't any default implementations. However, I'm proud of the workarounds I came-up with given
the set of self-imposed restrictions I was working under. Overall I had a great time problem solving and learning more
on how to utilize the tools in front of me, but would be even more excited to do this in a back-and-forth collaborative
environment that aims to find the optimal UI/UX solution!
