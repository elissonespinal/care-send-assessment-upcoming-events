<!-- Component representing the whole processed event page -->

<template>
  <!-- Page container -->
  <div class="container box">
    <!-- A little page label -->
    <h1 class="is-size-3 has-text-left block">
      Upcoming Events
    </h1>
    <div>
      <br>
      <!-- A container for the user-input that changes the display of event data  -->
      <section class="section has-background-info-light">
        <div
          style="max-width: 40rem; min-height: max-content"
          class="level has-background-primary-10 m-auto p-10 box"
        >
          <!-- The two switches to data a day at a time and/or in table/timeline mode -->
          <div
            class="level-item ml-auto mr-auto mb-15"
            style="max-width: fit-content"
          >
            <b-field
              label="Viewing Options"
              class="is-flex is-flex-direction-column block"
            >
              <!-- A day at a time or range -->
              <b-switch
                v-model="showSingleDay"
                class="is-flex-direction-column"
                @input="onSingleDayChange"
              >
                Show a Day at a Time
              </b-switch>
              <!-- table/timeline mode -->
              <b-switch
                v-model="showAsTable"
                class="is-flex-direction-column"
              >
                Show as Table
              </b-switch>
            </b-field>
          </div>
          <!-- Section for the datepicker element to select data ber day -->
          <div
            class="level-item m-auto"
            style="max-width: fit-content;"
          >
            <b-field
              :label="dateSelectionLabel"
              class="is-flex is-flex-direction-column block"
            >
              <b-datepicker
                v-model="selectedRange"
                :focusedDate="minDateVal"
                :minDate="minDateVal"
                :maxDate="maxDateVal"
                :range="!showSingleDay"
                :mobileNative="false"
                placeholder="Click to select..."
              />
            </b-field>
          </div>
        </div>
      </section>
      <section class="section">
        <!-- Show the either of the following based off of user-input -->
        <!-- Our custom EventsTable component -->
        <EventsTable
          v-if="showAsTable"
          :event-data-obj="dayEventMapIterable"
        />
        <!-- Our custom EventsTimeline component -->
        <EventsTimeline
          v-else
          :event-data-obj="dayEventMapIterable"
        />
      </section>
    </div>
  </div>
</template>

<script lang="ts">
import IntervalTree from '@flatten-js/interval-tree';
import { defineComponent } from '@vue/composition-api';
import { parse, startOfDay, eachDayOfInterval, addDays, isSameDay } from 'date-fns';

import data from '@/assets/data/mock.json';
import EventsTable from '@/components/EventsTable.vue';
import EventsTimeline from '@/components/EventsTimeline.vue';
import { ProcessedEvent, Event, UpcomingEventsData } from '@/utils/types';

// This is the format of data on the mock.json file
const dateFormatString = 'MMMM d\',\' y p';

// data pre-processing:
// Pre-process end/start strings into Date objects -> ProcessedEvent w/ conflict var
// -> sort based off of event start-time in chronological order
const sortedMockData = (data as Event[]).map(
  // Map into ProcessedEvent
  ({ title, start, end }): ProcessedEvent => ({
    title,
    start: parse(start, dateFormatString, new Date()),
    end: parse(end, dateFormatString, new Date()),
    conflict: false,
  }),
).sort( // Sort!
  (e1, e2) => e1.start.valueOf() - e2.start.valueOf(),
);

// This component
export default defineComponent({
  name: 'UpcomingEvents',
  components: {
    EventsTable,
    EventsTimeline,
  },
  data(): UpcomingEventsData {
    return {
      sortedData: sortedMockData, // our pre-processed data
      selectedRange: startOfDay(sortedMockData[0].start), // User-selected data
      showSingleDay: true, // bound state from showSingleDay switch
      showAsTable: true, // bound state from showAsTable switch
      dayEventsMap: {}, // Maps each day to its ProcessedEvents after checking conflicts
      nextEventIdx: 0, // Last event we processed into day-conflict map
    };
  },
  // Make sure to run conflict algo on all events before mounting component
  beforeMount() {
    this.processAllEvents();
  },
  computed: {
    // Filters event lists based on user-selected dates
    dayEventMapIterable(): { day: Date; events: ProcessedEvent[] }[] {
      if (this.showSingleDay && this.selectedRange instanceof Date) {
        return [{
          day: this.selectedRange,
          events: this.dayEventsMap[startOfDay(this.selectedRange).valueOf()],
        }];
      } if (this.selectedRange instanceof Array) {
        return eachDayOfInterval(
          { start: this.selectedRange[0], end: this.selectedRange[1] },
        ).map((day) => ({ day, events: this.dayEventsMap[day.valueOf()] }));
      }
      return [];
    },
    // helper that returns the first date of our data
    minDateVal(): Date {
      return startOfDay(this.sortedData[0].start);
    },
    // helper that returns the last date of our data
    maxDateVal(): Date {
      return startOfDay(this.sortedData[this.sortedData.length - 1].end);
    },
    // Datepicker label if in single-day or range mode
    dateSelectionLabel(): string {
      return this.showSingleDay ? 'Select a Date' : 'Select a Date Range';
    },
  },
  methods: {
    // Check if an event overlaps with any other event in the running event list
    // Remove any events that have reached their end date from the runningEvents set
    // Return bool to assign event.conflict = bool (airbnb style doesn't like params
    // being modified in place)
    updateConflictsAndRunningEvents(
      event: ProcessedEvent,
      runningEvents: IntervalTree<ProcessedEvent>,
    ): void {
      const interval: [start: number, end: number] = [
        event.start.valueOf(),
        event.end.valueOf(),
      ];

      // It's just not necessary to store the data globally so just
      // ignore the eslint rule
      /* eslint-disable no-param-reassign */
      runningEvents.search(interval, (event2: ProcessedEvent) => {
        // Avoid flagging overlapping start/end times as a conflict
        if (event2.end.valueOf() === event.start.valueOf()) return;
        [event.conflict, event2.conflict] = [true, true];
      });
      runningEvents.insert(interval, event);
      /* eslint-enable no-param-reassign */
    },
    // Process the next day's worth of events in the sorted data
    processNextDay(): void {
      // skip if done
      if (this.nextEventIdx >= this.sortedData.length) return;

      let e: ProcessedEvent = this.sortedData[this.nextEventIdx];
      const day: Date = startOfDay(e.start);
      const eventsInDay: ProcessedEvent[] = [];
      const runningEvents = new IntervalTree<ProcessedEvent>();

      // Keep processing unless you run out of events or if you reach the next day
      while (this.nextEventIdx < this.sortedData.length && isSameDay(e.start, day)) {
        this.updateConflictsAndRunningEvents(e, runningEvents);
        eventsInDay.push(e);
        this.nextEventIdx += 1;
        e = this.sortedData[this.nextEventIdx];
      }
      // Using vue's $set to make sure the reactivity system is aware of the change
      this.$set(this.dayEventsMap, day.valueOf(), eventsInDay);
    },
    // call processing until you've done all days/events
    processAllEvents(): void {
      while (this.nextEventIdx < this.sortedData.length) {
        this.processNextDay();
      }
    },
    // update dates on the date picker when switch for range-mode changes
    onSingleDayChange(v: boolean): void {
      if (v && this.selectedRange instanceof Array) {
        [this.selectedRange] = this.selectedRange;
      } else if (!v && this.selectedRange instanceof Date) {
        this.selectedRange = [
          startOfDay(this.selectedRange),
          addDays(startOfDay(this.selectedRange), 1),
        ];
      }
    },
  },
});
</script>
