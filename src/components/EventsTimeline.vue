<!-- A timeline-style display of occurring events on a given set of days -->
<template>
  <section class="box">
    <!-- Go through all of the post-processed input timeline data -->
    <div
      v-for="[day, timelines] in timelineData"
      :key="day.valueOf()"
      class="content"
    >
      <!-- Only render if non-empty -->
      <div
        v-if="timelines.length"
        class="block"
      >
        <!-- Label the day the data is for -->
        <h2 class="is-size-3 has-text-left">
          Events for {{ getFormattedDay(day) }}
        </h2>
        <!-- Get each sub-timeline so you can show overlaps -->
        <div
          v-for="(tl, i) in timelines"
          :key="i"
          class="block"
        >
          <!-- Show tags to tell what task is what -->
          <b-field
            grouped
            groupMultiline
          >
            <div
              v-for="item in tl.filter(({title}) => title)"
              :key="`${item.title}`"
              class="control"
            >
              <b-taglist
                attached
              >
                <b-tag
                  type="is-dark"
                  class="has-text-weight-bold is-small is-unselectable"
                >
                  {{ `${item.title}`.at(0) }}
                </b-tag>
                <b-tag class="is-small">
                  {{ `${item.title}` }}
                </b-tag>
              </b-taglist>
            </div>
          </b-field>
          <!-- Main Timeline Element + Container -->
          <div class="container">
            <b-progress
              :max="1440"
              size="is-large"
              :rounded="false"
              class="mb-0"
            >
              <!-- Custom stacked progress bar -> shows each task/empty space -->
              <template
                #bar
              >
                <b-progress-bar
                  v-for="(it, j) in tl"
                  :key="j"
                  :showValue="true"
                  :value="it.value"
                  :type="getConflictStyle(it)"
                  :aria-label="it.title"
                  style="border-left: 0.5px solid hsl(0, 0%, 96%)"
                  format="raw"
                >
                  <!-- Single-letter label -->
                  <span class="is-unselectable has-text-black">
                    {{ it.title?.at(0) || '' }}
                  </span>
                </b-progress-bar>
              </template>
            </b-progress>
            <!-- Custom time-ticks -->
            <div
              class="
                columns is-mobile is-flex-direction-columns is-align-content-stretch m-0
              "
            >
              <!-- Custom ticks with Bulma columns; HIDDEN on mobile-size -->
              <div
                v-for="hr in 24"
                :key="hr"
                style="min-width: fit-content;"
                class="column is-flex is-align-content-stretch p-0 m-0 is-hidden-mobile"
              >
                <span
                  style="font-size: 7px; text-wrap: nowrap;"
                  class="has-text-left is-flex is-align-content-stretch"
                >
                  {{ timeGridTick(hr - 1, 1) }}
                </span>
              </div>
              <!-- Ticks on every-other hour for mobile-size -->
              <div
                v-for="hr in 12"
                :key="`small${hr}`"
                style="min-width: fit-content;"
                class="column is-flex is-align-content-stretch p-0 m-0 is-hidden-tablet"
              >
                <span
                  style="font-size: 7px; text-wrap: nowrap;"
                  class="has-text-left is-flex is-align-content-stretch"
                >
                  {{ timeGridTick(hr - 1, 2) }}
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</template>

<script lang='ts'>
import { defineComponent, PropType } from '@vue/composition-api';
import { format, differenceInMinutes, startOfDay } from 'date-fns';

import { ProcessedEvent, TimelineItem } from '@/utils/types';

// convert date to minutes (from 12:00 am from same day)
const getTotalMins = (date: Date): number => (
  differenceInMinutes(date, startOfDay(date))
);

// Constructor for each timeline item (either an event or empty space between events)
const genTimelineItem = (value: number, title: string | null, conflict: boolean)
    : TimelineItem => ({ value, title, conflict });

// this component
export default defineComponent({
  name: 'EventsTimeline',
  props: {
    // The only prop: the list days with each of their ProcessedEvent lists
    eventDataObj: {
      required: true,
      type: Array as PropType<{ day: Date; events: ProcessedEvent[] }[]>,
    },
  },
  computed: {
    // Process the this.eventDataObj prop into a list of [Date: TimelineItem[][]] pairs
    // that represent the day the events happen in, and the sub-timelines that display
    // the events
    timelineData(): [Date, TimelineItem[][]][] {
      return this.eventDataObj.map(
        ({ day, events }) => (
          events ? [day, this.eventToGraphSections(events)] : [day, []]
        ),
      );
    },
  },
  methods: {
    // Generates a string label of the form H:MM ['AM' | 'PM']
    timeGridTick(hr: number, skip: number): string {
      return `${((hr * skip + 12) % 12) || 12} ${hr * skip < 12 ? 'AM' : 'PM'}`;
    },
    // Formats a Date into a simple Month day, Year format
    getFormattedDay(date: Date): string {
      return format(date, 'MMMM d\',\' y');
    },
    // Returns the color style for either empty space, conflicting, or 'a-okay'
    // timeline items
    getConflictStyle(timelineIt: TimelineItem): string {
      if (!timelineIt.title) return 'is-light';
      return timelineIt.conflict ? 'is-danger' : 'is-success';
    },
    // Generates the progress-bars that will visually represent the events on a list of
    // ProcessedEvent objects
    eventToGraphSections(events: ProcessedEvent[]): TimelineItem[][] {
      const bars: { barItems: TimelineItem[], sum: number }[] = [
        { barItems: [], sum: 0 },
      ];
      events.forEach((e) => {
        let barToUse = bars[0];
        const currMinsStart = getTotalMins(e.start);
        const currMinsEnd = getTotalMins(e.end);
        // Find the bar to use (must have space for event) or create a new one
        if (barToUse.sum > currMinsStart) {
          barToUse = bars.find((b) => b.sum <= currMinsStart)
                     || { barItems: [], sum: 0 };
          if (!(barToUse === bars[bars.length - 1])) {
            bars.push(barToUse);
          }
        } // Add empty space if needed
        if (currMinsStart > barToUse.sum) {
          barToUse.barItems.push(genTimelineItem(
            currMinsStart - barToUse.sum, null, false,
          ));
          barToUse.sum = currMinsStart;
        } // Add event
        barToUse.barItems.push(genTimelineItem(
          currMinsEnd - currMinsStart, e.title, e.conflict,
        ));
        barToUse.sum = currMinsEnd;
      });
      bars.forEach(
        (b) => {
          if (b.sum < 1440) {
            b.barItems.push(genTimelineItem(1440 - b.sum, '', false));
          }
        },
      );
      return bars.map(({ barItems }) => barItems);
    },
  },
});
</script>
