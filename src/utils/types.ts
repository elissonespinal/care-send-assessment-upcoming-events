// General file I made so that I didn't end-up with weird cyclical dependencies
// Placed other types here so the file isn't empty

// transformed events from the mock.json file to include a conflict bool
// and the start and end times are Date objects
export interface ProcessedEvent {
  title: string;
  start: Date;
  end: Date;
  conflict: boolean;
}

// The base event interface format straight from the mock data
export interface Event {
  title: string;
  start: string;
  end: string;
}

// The data field for the UpcomingEvents component
export interface UpcomingEventsData {
  sortedData: ProcessedEvent[];
  selectedRange: Date | [Date, Date];
  showSingleDay: boolean;
  showAsTable: boolean;
  dayEventsMap: { [key: number]: ProcessedEvent[] };
  nextEventIdx: number;
}

// The data field for the TimelineItem component
export interface TimelineItem {
  value: number; // Holds the length of the event (or space between events) in mins
  title: string | null;
  conflict: boolean;
}
